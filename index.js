// console.log("Hello World!");

// Syntax, Statements and Comments

// Statement in programming are instructions that we tell the computer to perform.
// We use semicolon(;) to end a statement. (JS doesnt require semicolons)

// Syntax in programming, it is the set of rules on how statements must be constructed.

// Comments are meant to describe the written code. (Documentation)

/* 
    There are two types of comments in JS:
        1. Single line comments denoted by two slashes; Windows: (ctrl+/), Mac: (cmd+/)
        2. Multi-line comments denoted by slash and asterisk; Windows: (ctrl+shift+/), Mac: (cmd+option+/)
*/

// Variables
// It is used to conntain data.

// Declaring a variable.
// It tells our devices that a variable name is Created and is ready to store data.

/* 
    Syntax:
        let/const variableName;

*/

// let myVariable;

// console.log(myVariable);

// hello = "Hi"

let hello = "Hi Batch 197!";

console.log(hello);

//

let productName = "Desktop Computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const PI = 3.1416;
console.log(PI);

console.log("");

// Reassigning Variable values
// Changing the initial or previous value into another value

/* 
    Syntax:
        variableName = newValue;

*/

productName = "laptop";
console.log(productName);

// Will return an error because we tried reassigning a value to a constant variable.
/* PI = 3.1415;
console.log(PI); */

// Reassigning vs Initializing Variables.

let supplier;

// Initializing
// assign the initial value.

supplier = "John Tradings";
console.log(supplier);

// Reassigning Variable
// Change the initial value of the variable.
supplier = "Zuitt Store";
console.log(supplier);

// Reserve Keywords as variable

// Using a reserved keyword will cause a syntax error.
/* const var = "hello";
console.log(let); */

// Data Types

// String
// String data type is a series of characters that create a word, a phrase, a sentence, or anything related to creating a text.
// JS Strings can be written using either Double Quote ("") or Single Quote ('').

let country = "Philippines";
let province = "Metro Manila";

console.log(country, province);

// Concatenating Strings
// Multiple String values can be combined to create a single string using the "+" symbol.

let fullAddress = "I live in " + province + ", " + country;
console.log(fullAddress);

/*
	Mini Activity Instructions:
		1. Create variables that will contain your firstName and lastName.
		2. Reassign the value of the province variable with the province you live in.
		3. Reassign the value of the fullAddress variable with this sentence:
			Hello, I am firstName lastName. I live in province, country.
			Example:
				Hello, I am Angelito Quiambao. I live in Tarlac, Philippines.
		4. Print the new value of the greeting variable using console log.
		5. Once done, take a screenshot of your browser console and send it to the batch hangouts.
*/

let firstName = "Levin";
let lastName = "Alvarez";
province = "Zamboanga";
fullAddress =
  "Hello, I am " +
  firstName +
  " " +
  lastName +
  ". I live in " +
  province +
  ", " +
  country +
  ".";

console.log(fullAddress);

// Escape Characters (\) in combition with string characters to produce a different result.
// \n creates a new line between text
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = "John\\'s employees went home early";
console.log(message);

console.log(" ");
// Number
// Integer/Whole

let headCount = 19;
console.log(headCount);

// Decimal/Fraction
let grade = 96.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining number and string
console.log("John's grade in Math last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things.
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// Arrays are a special kind of data type that is used to store multiple values with the same data type.

let grade1 = 92.5,
  grade2 = 97.5,
  grade3 = 89.3,
  grade4 = 96.2;

console.log(grade1, grade2, grade3, grade4);
/* 
    Syntax:
        let/const arrayName = [elementA, elementB, elementC];

*/

//0 1 2 3
let grades = [92.5, 97.5, 89.3, 96.2];
console.log(grades);

// Array index is a number used to identify the place/order of an element in an array.

// Array elements usually starts with 0 to n.
// to identify the last element in an array: arrayName.length-1

console.log(grades[3]);
console.log(grades[grades.length - 1]);

// Array with different data types
// This is not always recommended

let details = ["John", "Smith", 32, true];
console.log(details);

// Object
// Objects are another special kind of data type that is used to mimic real world objects/items.

/* 
    Sytnax:
        let/conts objectName = {
            propertyA: valueA,
            propertyB: valueB
        };


*/

let person = {
  fullName: "Juan Dela Cruz",
  age: 35,
  isMarried: false,
  contactNo: ["+63917 123 4567", "8123 4567"],
  address: {
    houseNumber: "36-A",
    barangay: "Ginebra",
    city: "Manila",
    country: "Philippines",
  },
};

console.log(person);

// Selecting a property of an objet
// dot notation is used to access a specific property of an object.

console.log(person.fullName);
console.log(person.isMarried);

// Object property within an object
console.log(person.address.city);
// Object with array
console.log(person.contactNo[0]);

// Abstract Objects
let myGrades = {
  Math: 98.7,
  Science: 92.1,
  History: 90.2,
  English: 94.6,
};

console.log(myGrades);

// Null and undefined
// Null is an assigned value but it means nothing.
// Undefined is a value result from a variable declared without initialization.

// Null
let spouse = null;
console.log(spouse);

// Undefined
let name;
console.log(name);
